const token = require('../helper/token');
const expect = require('chai').expect;

describe('Token', () => {
	it('Generate new token should have default string length', () => {
		expect(token.generateNew().length).to.be.equal(32);
	});

	it('Generate new token can have different string length depends on the parameters', () => {
		const lengths = [8, 4, 4, 4, 12];
		const separator = '-';
		const expected_length = lengths.reduce((sum, each_length) => (sum + each_length), 0) + (lengths.length - 1) * (separator.length);
		expect(token.generateNew(lengths, separator).length).to.be.equal(expected_length);
	});
});