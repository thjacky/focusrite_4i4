const tsp = require('../helper/tsp');
const expect = require('chai').expect;

describe('Token', () => {
  it('Tsp return a success result', function* () {
    const result = yield tsp.shortestPath([
      [22.372081, 114.107877],
      [22.284419, 114.159510],
      [22.326442, 114.167811]
    ]);
    expect(result.status).to.be.equal('success');
  });

  it('Tsp return a failure result', function* () {
    const result = yield tsp.shortestPath([
      [22.372081, 114124.107877],
      [22.284419, 114124.159510],
      [22.326442, 114124.167811]
    ]);
    expect(result.status).to.be.equal('failure');
  });
});