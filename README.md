# Focusrite backend

I. Use `docker-compose.yml` to create the MySQL database.

II. Before starting the backend, please change the value of `database.host` in `./config/default.js` if the container's IP address is **not** `192.168.99.100`.