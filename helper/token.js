'use strict';
const randomString = require('randomstring');

const token_exports = {
  generateNew: (lengths = [32], separator = '') => (
    lengths.map(length => randomString.generate({
      length: length,
      charset: '123456789012345678901234567890abcdefghijklmnopqrstuvwxyz'
    })).join(separator)
  )
}

module.exports = token_exports;