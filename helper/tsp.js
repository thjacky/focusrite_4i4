'use strict';
const requestPromise = require('request-promise');
const randomString = require('randomstring');

function getMatrices(locations_array) {
  const locations_string = locations_array.map(location => location.join(',')).join('|');
  var distance_matrix = [],
      duration_matrix = [];
  return requestPromise({
    uri: 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='+locations_string+'&destinations='+locations_string+'&mode=driving&language=en',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true
  }).then(repos => {
    if (repos.status === 'OK') {
      for (let i = 0; i < repos.rows.length; i++) {
        var each_row = repos.rows[i],
            distance_matrix_row = [],
            duration_matrix_row = [];
        for (var j = 0; j < each_row.elements.length; j++) {
          var each_element = each_row.elements[j];
          if (each_element.status === 'OK') {
            distance_matrix_row.push(each_element.distance.value);
            duration_matrix_row.push(each_element.duration.value);
          } else {
            return {
              status: each_element.status,
              distance_matrix: distance_matrix,
              duration_matrix: duration_matrix
            };
          }
        }
        distance_matrix.push(distance_matrix_row);
        duration_matrix.push(duration_matrix_row);
      }
    }
    return {
      status: repos.status,
      distance_matrix: distance_matrix,
      duration_matrix: duration_matrix
    };
  }).catch(err => {
    return {
      status: 'Server error, please try again later.',
      distance_matrix: distance_matrix,
      duration_matrix: duration_matrix
    };
  });
}

function minCostPath(paths) {
  return paths.reduce((min_path, each_path) => {
    if (min_path.length === 0 || min_path.cost > each_path.cost) {
      return each_path;
    }
    return min_path;
  }, []);
}

function getMinCostPath(end_at, pass_through, distance_matrix, duration_matrix) {
  var paths = [];
  if (pass_through.length === 0) {
    return {
      cost:     distance_matrix[0][end_at],
      duration: duration_matrix[0][end_at],
      node:     end_at,
      path:     []
    };
  }
  for (var i = pass_through.length - 1; i >= 0; i--) {
    const each_node = pass_through[i],
          pmc = getMinCostPath(each_node, pass_through.filter((node, k) => (k !== i)), distance_matrix, duration_matrix);
    paths.push({
      cost:     pmc.cost + distance_matrix[each_node][end_at],
      duration: pmc.duration + duration_matrix[each_node][end_at],
      node:     end_at,
      path:     pmc.path.concat([pmc.node])
    });

  }
  return minCostPath(paths);
}

const path_status = {
  INVALID_REQUEST: 'Invalid request, please try again later.',
  MAX_ELEMENTS_EXCEEDED: 'Too many locations, please remove some of them.',
  OVER_QUERY_LIMIT: 'Submit Too many times, please try again later.',
  REQUEST_DENIED: 'Service denied.',
  UNKNOWN_ERROR: 'Unknown error occurred, please try again later.',
  NOT_FOUND: 'Locations not found.',
  ZERO_RESULTS: 'Locations not accessible by car.',
  MAX_ROUTE_LENGTH_EXCEEDED: 'Locations are too far apart.'
}

const tsp_exports = {
  shortestPath: (locations_array) => (
    getMatrices(locations_array).then(results => {
      const {status, distance_matrix, duration_matrix} = results;
      if (status === 'OK') {
        var paths = [];
        const pass_through = distance_matrix.map((row, i) => i).filter((node, i) => (i !== 0));
        for (var i = pass_through.length - 1; i >= 0; i--) {
          const each_node = pass_through[i];
          paths.push(getMinCostPath(each_node, pass_through.filter((node, k) => (k !== i)), distance_matrix, duration_matrix));
        }
        var tsp = minCostPath(paths);
        tsp.path.unshift(0);
        tsp.path.push(tsp.node);
        return {
          status:         'success',
          path:           tsp.path.map((path) => locations_array[path]),
          total_distance: tsp.cost,
          total_time:     tsp.duration
        };
      } else {
        return {
          status: 'failure',
          error:  path_status[status]
        };
      }
    })
  )
};

module.exports = tsp_exports;