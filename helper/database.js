'use strict';
const config = require('../config/default');
const mysql = require('mysql');
const connection = mysql.createConnection(config.database);
const async = require('async');
const randomString = require('randomstring');
const token = require('./token');

const locations = `CREATE TABLE IF NOT EXISTS locations (
 id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 token VARCHAR(36) NOT NULL,
 result TEXT
);`;

function query(sql, values = []) {
  return new Promise((resolve, reject) => {
    connection.beginTransaction((error) => {
      if (error) {
        reject(error);
      }
      connection.query(sql, values, (error, results, fields) => {
        if (error) {
          return connection.rollback(() => {
            reject(error);
          });
        }
        connection.commit((error) => {
          if (error) {
            return connection.rollback(() => {
              reject(error);
            });
          }
          resolve({values: values, rows: results});
        });
      });
    });
  });
}

query(locations); // create table

const db_exports = {
  getNewToken: (lengths = [32], separator = '') => (
    new Promise((resolve, reject) => {
      resolve(token.generateNew(lengths, separator));
    }).then((token) => {
      return query('SELECT * FROM locations WHERE ?', {token: token});
    }).then((results) => {
      if (results.rows.length > 0) {
        return db_exports.getNewToken(lengths, separator);
      } else {
        return query('INSERT INTO locations SET ?', {token: results.values.token, result: JSON.stringify({status: 'in progress'})});
      }
    })
  ),
  saveResultTo: (token, values) => {
    return query('UPDATE locations SET ? WHERE token = ?', [values, token]);
  },
  getResultFrom: (token) => {
    return query('SELECT * FROM locations WHERE ?', {token: token})
  }
};

module.exports = db_exports;