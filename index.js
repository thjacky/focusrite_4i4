'use strict';
const app = require('koa')();
const cors = require('koa-cors');
const logger = require('koa-logger');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const randomString = require('randomstring');
const config = require('./config/default');
const tsp = require('./helper/tsp');
const database = require('./helper/database');

router.post('/route', function* () {
  var _request = this.request,
      _response = this.response;
  try {
    const token = yield database.getNewToken([8, 4, 4, 4, 12], '-').then(results => results.values.token);
    this.body = {
      token: token
    };
    tsp.shortestPath(_request.body).then(result => {
      database.saveResultTo(token, {result: JSON.stringify(result)});
    });
  } catch (error) {
    this.status = 500;
    this.body = {error: error};
  }
});

router.get('/route/:token', function* () {
  var _params = this.params,
      _response = this.response;
  try {
    this.body = yield database.getResultFrom(_params.token)
    .then(results => {
      if (results.rows.length > 0) {
        return JSON.parse(results.rows[0].result);
      } else {
        return {
          status: 'failure',
          error: 'Invalid token.'
        };
      }
    });
  } catch (error) {
    this.status = 500;
    this.body = {error: error};
  }
});

app
  .use(logger())
  .use(cors())
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(config.port);
console.log('Mock server started at port '+config.port);